FROM maven:alpine AS build

ENV MAINZELLISTE_GIT_COMMIT=f1e11d29ecdf48b17c4ae8d0d4016b52d066f889

ADD https://bitbucket.org/medicalinformatics/mainzelliste/get/$MAINZELLISTE_GIT_COMMIT.zip /target/mainzelliste.zip

RUN cd /target && \
    unzip mainzelliste.zip && \
    cd medicalinformatics-mainzelliste-* && \
    mvn package && \
    mkdir -p /target/extracted && \
    mv target/*.war /target && \
    cd /target/extracted && \
    unzip /target/mainzelliste-*.war


FROM tomcat:8-jre8-alpine

RUN rm -r /usr/local/tomcat/webapps/* && \
    apk --no-cache add curl

COPY --from=build /target/extracted/ /usr/local/tomcat/webapps/ROOT/

# Supply a default config. This can be changed by mounting a volume at /etc/mainzelliste
ADD conf/ /etc/mainzelliste/

ADD ml_entrypoint.sh /ml_entrypoint.sh

HEALTHCHECK --interval=10s --timeout=2s --retries=2 CMD curl -f http://localhost:8080
ENTRYPOINT [ "/ml_entrypoint.sh" ]
